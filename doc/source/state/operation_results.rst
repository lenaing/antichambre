Operation Results
-----------------

.. automodule:: antichambre.state.operation_results
    :show-inheritance:
    :inherited-members:
    :members:
#######################################
Welcome to antichambre's documentation!
#######################################

*antichambre* is a `Terraform <https://www.terraform.io/>`_ HTTP remote
backend acting as a gateway to `Vault <https://www.vaultproject.io/>`_.

It enables storing Terraform states securely inside of Vault, therefore
protecting states' secrets from eavesdropping.

Disclaimer
----------

.. warning::

    This project is a proof of concept and **NOT** production ready.
    **DO NOT use it in production.**

Quickstart
----------

Make sure you have a *vault* server in development mode with a
root token ID set to ``dev-root-token-id``.

.. code-block:: console

    $ vault server -dev -dev-root-token-id dev-root-token-id

Install *antichambre* dependencies and run it.

.. code-block:: console

    $ pip install -r requirements.txt
    $ gunicorn antichambre.app:app

Create a *terraform* backend configuration using the *http* backend
and point it to *antichambre* specifying the name of your state.

.. code-block:: console

    $ cat <<EOF >backend.tf
    terraform {
      backend "http" {
        address            = "http://127.0.0.1:8000/v1/states/foo"
        workspaces = true
      }
    }
    EOF

Now, run ``terraform plan`` or ``terraform apply``, and it will use *antichambre* to store its state.


.. toctree::
   :caption: Contents:
   :hidden:

   state/index.rst
   vault/index.rst

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

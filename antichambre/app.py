"""Antichambre Falcon application"""
import re
import falcon
from .state.resource import StateResource
from .configuration import _load_config

_load_config()
api = app = falcon.API()
state_resource = StateResource()
api.add_sink(state_resource.sink_handler, re.compile(r'/v1/states/(?P<state>.*)'))
api.add_sink(state_resource.sink_handler, re.compile(r'/v1/locks/(?P<state>.*)'))
api.add_sink(state_resource.sink_handler, re.compile(r'/v1/workspaces/(?P<state>.*)'))

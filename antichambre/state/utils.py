"""
Terraform state utilities.
"""
import base64
import gzip
import os

def decode_state(state, encoding):
    """Decode an encoded Terraform state.

    Args:
        state (str): Terraform encoded state data.
        encoding (str|None): Encoding algorithm.

    Returns:
        str: The Terraform unencoded state data.
    """
    if encoding == "base64":
        return base64.b64decode(state)
    if encoding == "gzip+base64":
        return gzip.decompress(base64.b64decode(state))
    return state

def split_state_in_chunks(state, max_chunk_size):
    """Split a Terraform state in even chunks.

    Args:
        state (str): Terraform state data.
        max_chunk_size (int): Maximum chunk size.

    Returns:
        list(str): List of Terraform state data chunks.
    """
    return [state[i:i+max_chunk_size] for i in range(0, len(state), max_chunk_size)]

def create_secrets(state_data):
    """Create Vault secrets from a Terraform state.

    Args:
        state_data(str): Terraform state data.

    Returns:
        list(dict): List of Vault secrets dictionnaries.
    """
    vault_storage_max_object_size = int(os.environ["ANTICHAMBRE_VAULT_STORAGE_MAX_OBJECT_SIZE"])
    vault_backend = os.environ["ANTICHAMBRE_VAULT_STORAGE_BACKEND_TYPE"]
    gzip_enabled = os.environ["ANTICHAMBRE_VAULT_STORAGE_GZIP_ENABLED"] in ["True", "true"]
    gzip_default = os.environ["ANTICHAMBRE_VAULT_STORAGE_GZIP_DEFAULT"] in ["True", "true"]
    secrets = []

    if not vault_storage_max_object_size:
        if vault_backend == "consul":
            vault_storage_max_object_size = 524288

        if vault_backend == "integrated":
            vault_storage_max_object_size = 1048576

    state_len = len(state_data)

    if not vault_storage_max_object_size or state_len < vault_storage_max_object_size:
        # State is thin enough to be stored as is in Vault backend
        if gzip_enabled and gzip_default:
            secrets.append({
                'state': base64.b64encode(gzip.compress(state_data)).decode('utf-8'),
                'state_format': 'gzip+base64',
                'chunks': 1
            })
        else:
            secrets.append({
                'state': state_data.decode('utf-8'),
                'state_format': 'raw',
                'chunks': 1
            })
    else:
        # State is to large to be stored "as is" in Vault backend, split it in chunks
        if gzip_enabled:
            state_data = base64.b64encode(gzip.compress(state_data)).decode('utf-8')
            state_chunk_format = 'gzip+base64'
        else:
            state_data = base64.b64encode(state_data).decode('utf-8')
            state_chunk_format = 'base64'

        state_chunks = split_state_in_chunks(state_data, vault_storage_max_object_size)
        state_chunks_count = len(state_chunks)
        for state_part in state_chunks:
            secrets.append({
                'state': state_part,
                'state_format': state_chunk_format,
                'chunks': state_chunks_count
            })

    return secrets

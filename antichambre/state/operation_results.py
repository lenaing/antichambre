"""Results of an operation on a Terraform state stored in Vault."""


class OperationResult():
    """A result of an operation on a Terraform state stored in Vault.

    Attributes:
        state (str): Target Terraform state name.
        workspace (str): Target Terraform workspace name.
        operation (str): Operation that was performed on the state.
        error (str | None): Reason of operation failure if it failed.
                            `None` otherwise.
    """

    # pylint: disable=too-few-public-methods
    def __init__(self, state, workspace, operation, error=None):
        self.state = state
        self.workspace = workspace
        if not workspace:
            self.workspace = "default"
        self.operation = operation
        self.error = error


class RetrieveResult(OperationResult):
    """A Terraform state retrieval operation result.

    Attributes:
        found (bool): Was state found in Vault.
        state_data (str): State data.
    """

    # pylint: disable=too-few-public-methods,too-many-arguments
    def __init__(self, state, workspace, error=None, found=True,
                 state_data=None):

        super().__init__(state, workspace, "Retrieve", error=error)
        self.found = found
        self.state_data = state_data


class StoreResult(OperationResult):
    """A Terraform state store operation result."""

    # pylint: disable=too-few-public-methods
    def __init__(self, state, workspace, error=None):
        super().__init__(state, workspace, "Store", error=error)


class DeleteResult(OperationResult):
    """A Terraform state delete operation result."""

    # pylint: disable=too-few-public-methods
    def __init__(self, state, workspace, error=None):
        super().__init__(state, workspace, "Delete", error=error)


class LockResult(OperationResult):
    """A Terraform state lock operation result.

    Attributes:
        lock_info (str): Current lock info holder.
    """

    # pylint: disable=too-few-public-methods
    def __init__(self, state, workspace, error=None, lock_info=None):
        super().__init__(state, workspace, "Lock", error=error)
        self.lock_info = lock_info


class UnlockResult(OperationResult):
    """A Terraform state unlock operation result.

    Attributes:
        lock_info (str): Current lock info holder.
    """

    # pylint: disable=too-few-public-methods
    def __init__(self, state, workspace, error=None, lock_info=None):
        super().__init__(state, workspace, "Unlock", error=error)
        self.lock_info = lock_info


class WorkspacesResult(OperationResult):
    """A Terraform state workspaces list operation result.

    Attributes:
        workspaces (list(str)): Current known workspaces of this state.
    """

    # pylint: disable=too-few-public-methods
    def __init__(self, state, error=None, workspaces=None):
        super().__init__(state, None, "Workspaces", error=error)
        self.workspaces = workspaces

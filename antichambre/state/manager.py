"""Manage Terraform states in Vault."""
from ..vault.storage import VaultStorage
from .operation_results import (
    RetrieveResult, StoreResult, DeleteResult,
    LockResult, UnlockResult, WorkspacesResult
)
from .utils import decode_state, create_secrets
TF_STATE_NAME = "terraform.tfstate"


class StateManager():
    """Manage Terraform states in Vault."""

    def __init__(self):
        self.vault_storage = VaultStorage()

    def retrieve(self, state, workspace):
        """Retrieve a Terraform state from Vault.

        Args:
            state (str): Terraform state name.
            workspace (str): Terraform workspace name.

        Returns:
            RetrieveResult: The operation result.
        """
        chunk_path = f"{state}/{TF_STATE_NAME}"
        if workspace:
            chunk_path = f"{state}/workspaces/{workspace}/{TF_STATE_NAME}"

        read = self.vault_storage.read_secret(chunk_path)
        if read.error:
            error = f"Failed to read state chunk '{chunk_path}': {read.error}"
            return RetrieveResult(state, workspace, error)

        if not read.found:
            return RetrieveResult(state, workspace, found=False)

        # Read all other state chunks, retrieving them as necessary
        state_data = read.secret["state"]
        for chunk in range(1, read.secret["chunks"]):
            chunk_name = f"{TF_STATE_NAME}.{chunk}"
            chunk_path = f"{state}/{chunk_name}"
            if workspace:
                chunk_path = f"{state}/workspaces/{workspace}/{chunk_name}"

            read_chunk = self.vault_storage.read_secret(chunk_path)
            if read_chunk.error or not read_chunk.found:
                if not read_chunk.error:
                    error = "Chunk not found"
                else:
                    error = read_chunk.error
                return RetrieveResult(
                    state,
                    workspace,
                    error=f"Failed to read state chunk '{chunk_path}': {error}"
                )

            state_data += read_chunk.secret["state"]

        # Decode state if it was compressed
        state_data = decode_state(state_data, read.secret["state_format"])

        return RetrieveResult(state, workspace, state_data=state_data)

    def _delete_state_chunks(self, state, workspace, end, start=0):
        errors = []
        for chunk in range(start, end):
            chunk_name = f"{TF_STATE_NAME}.{chunk}"
            if chunk == 0:
                chunk_name = TF_STATE_NAME

            chunk_path = f"{state}/{chunk_name}"
            if workspace:
                chunk_path = f"{state}/workspaces/{workspace}/{chunk_name}"

            delete = self.vault_storage.delete_secret(chunk_path)
            if delete.error:
                errors.append(("Failed to delete state chunk "
                              f"'{chunk_path}': {delete.error}"))

        return errors

    def store(self, state, workspace, state_data):
        """Store a state in Vault.

        Args:
            state (str): Terraform state name.
            workspace (str): Terraform workspace name.
            state_data (str): Terraform state data.

        Returns:
            StoreResult: The operation result.
        """

        # Get previous state chunks count
        old_chunks_count = 0
        chunk_path = f"{state}/{TF_STATE_NAME}"
        if workspace:
            chunk_path = f"{state}/workspaces/{workspace}/{TF_STATE_NAME}"

        read = self.vault_storage.read_secret(chunk_path)
        if read.error:
            return StoreResult(
                state,
                workspace,
                error=f"Failed to read previous state chunks: {read.error}"
            )

        if read.found:
            old_chunks_count = read.secret["chunks"]

        # Store new state secrets
        state_secrets = create_secrets(state_data)
        chunks_count = len(state_secrets)
        for idx, secret in enumerate(state_secrets):
            chunk_name = f"{TF_STATE_NAME}.{idx}"
            if idx == 0:
                chunk_name = TF_STATE_NAME
            chunk_path = f"{state}/{chunk_name}"
            if workspace:
                chunk_path = f"{state}/workspaces/{workspace}/{chunk_name}"

            store = self.vault_storage.store_secret(
                path=chunk_path,
                secret=secret
            )
            if store.error:
                return StoreResult(
                    state,
                    workspace,
                    error=(("Failed to store state chunk "
                           f"{chunk_path}: {store.error}"))
                )

        # Delete remnant chunks if any
        deletion_errors = self._delete_state_chunks(
            state,
            workspace,
            old_chunks_count,
            start=chunks_count
        )
        if deletion_errors:
            return StoreResult(
                state,
                workspace,
                error=f"Failed to remove old state chunks: {deletion_errors}"
            )

        return StoreResult(state, workspace)

    def delete(self, state, workspace):
        """Delete a state in Vault.

        Args:
            state (str): Terraform state name.
            workspace (str): Terraform workspace name.

        Returns:
            DeleteResult: The operation result.
        """

        # Get state chunks count
        secret_path = f"{state}/{TF_STATE_NAME}"
        if workspace:
            secret_path = f"{state}/workspaces/{workspace}/{TF_STATE_NAME}"

        read = self.vault_storage.read_secret(secret_path)
        if read.error:
            return DeleteResult(
                state,
                workspace,
                error=f"Failed to read previous state chunks: {read.error}"
            )

        if not read.found:
            return DeleteResult(
                state,
                workspace,
                error="Failed to find state."
            )

        # Delete all state chunks
        deletion_errors = self._delete_state_chunks(
            state,
            workspace,
            read.secret["chunks"]
        )
        if deletion_errors:
            return DeleteResult(
                state,
                workspace,
                error=f"Failed to remove old state chunks: {deletion_errors}"
            )

        return DeleteResult(state, workspace)

    def lock(self, state, workspace, lock_info):
        """Lock a state in Vault.

        Args:
            state (str): Terraform state name.
            workspace (str): Terraform workspace name.
            lock_info (str): Terraform new lock info.

        Returns:
            LockResult: The operation result.
        """

        lock_path = f"{state}/lock"
        if workspace:
            lock_path = f"{state}/workspaces/{workspace}/lock"

        read = self.vault_storage.read_secret(lock_path)
        if read.error:
            # Failed to read lock information
            return LockResult(state, workspace, error=read.error)
        if read.found:
            # State already locked
            return LockResult(
                state,
                workspace,
                error="State already locked",
                lock_info=read.secret["lock_info"]
            )

        store = self.vault_storage.store_secret(
            path=lock_path,
            secret={'lock_info': lock_info.decode('utf-8')}
        )
        if store.error:
            # Failed to store lock
            return LockResult(state, workspace, error=store.error)

        return LockResult(state, workspace)

    def unlock(self, state, workspace, lock_info):
        """Unlock a state in Vault.

        Args:
            state (str): Terraform state name.
            workspace (str): Terraform workspace name.
            lock_info (str): Terraform expected lock info.

        Returns:
            UnlockResult: The operation result.
        """

        lock_path = f"{state}/lock"
        if workspace:
            lock_path = f"{state}/workspaces/{workspace}/lock"

        read = self.vault_storage.read_secret(lock_path)
        if read.error:
            # Failed to read lock information
            return UnlockResult(state, workspace, error=read.error)

        if not read.found:
            # Lock not found
            return UnlockResult(state, workspace)

        old_lock_info = None
        if "lock_info" in read.secret:
            old_lock_info = read.secret["lock_info"].encode('utf-8')

        if old_lock_info and lock_info and old_lock_info != lock_info:
            # Fail if current lock info differs from given lock info
            return UnlockResult(
                state,
                workspace,
                lock_info=old_lock_info,
                error="Lock info differs"
            )

        delete = self.vault_storage.delete_secret(lock_path)
        if delete.error:
            # Failed to delete lock
            return UnlockResult(state, workspace, error=delete.error)

        return UnlockResult(state, workspace)

    def workspaces(self, state):
        """List available workspaces for a state in Vault.

        Args:
            state (str): Terraform state name.

        Returns:
            WorkspacesResult: The operation result.
        """

        workspaces_path = f"{state}/workspaces"
        read = self.vault_storage.list_secrets(workspaces_path)
        if read.error:
            # Failed to read workspaces information
            return WorkspacesResult(state, error=read.error)

        workspaces = [workspace[:-1] for workspace in read.secrets_list]

        return WorkspacesResult(state, workspaces=workspaces)

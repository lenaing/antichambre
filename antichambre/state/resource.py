"""Antichambre Falcon resource."""
import json
import falcon
from .manager import StateManager

class StateResource():
    """Antichambre Falcon resource to handle Terraform states operations."""

    def __init__(self):
        self.manager = StateManager()

    def sink_handler(self, req, resp, state):
        """Handle State Resource requests.

        Args:
            req (object): Terraform HTTP request.
            resp (object): Antichambre HTTP response.
            state (str): Terraform state path.
        """
        methods_handlers = {
            "GET": self.on_get,
            "POST": self.on_post,
            "DELETE": self.on_delete,
            "LOCK": self.on_lock,
            "UNLOCK": self.on_unlock,
            "OPTIONS": self.on_options
        }
        handler = methods_handlers.get(req.method)
        if not handler:
            resp.status = falcon.HTTP_BAD_REQUEST
            return
        handler(req, resp, state)

    def on_get(self, req, resp, state):
        """Handle State Resource GET requests.

        Args:
            req (object): Terraform HTTP request.
            resp (object): Antichambre HTTP response.
            state (str): Terraform state path.
        """
        workspace = req.params.get("workspace")

        retrieve = self.manager.retrieve(state, workspace)
        if retrieve.error:
            resp.body = retrieve.error
            resp.status = falcon.HTTP_INTERNAL_SERVER_ERROR
            return

        if retrieve.found and retrieve.state_data:
            resp.body = retrieve.state_data
            resp.status = falcon.HTTP_OK
            return

        resp.status = falcon.HTTP_NOT_FOUND

    def on_post(self, req, resp, state):
        """Handle State Resource POST requests.

        Args:
            req (object): Terraform HTTP request.
            resp (object): Antichambre HTTP response.
            state (str): Terraform state path.
        """
        workspace = req.params.get("workspace")
        state_length = req.content_length
        state_data = req.stream.read(state_length or 0)

        store = self.manager.store(state, workspace, state_data)
        if store.error:
            resp.body = store.error
            resp.status = falcon.HTTP_INTERNAL_SERVER_ERROR
            return

        resp.status = falcon.HTTP_OK

    def on_delete(self, req, resp, state):
        """Handle State Resource DELETE requests.

        Args:
            req (object): Terraform HTTP request.
            resp (object): Antichambre HTTP response.
            state (str): Terraform state path.
        """
        workspace = req.params.get("workspace")

        delete = self.manager.delete(state, workspace)
        if delete.error:
            resp.body = delete.error
            resp.status = falcon.HTTP_INTERNAL_SERVER_ERROR
            return

        resp.status = falcon.HTTP_OK

    def on_lock(self, req, resp, state):
        """Handle State Resource LOCK requests.

        Args:
            req (object): Terraform HTTP request.
            resp (object): Antichambre HTTP response.
            state (str): Terraform state path.
        """
        workspace = req.params.get("workspace")
        lock_info = req.stream.read(req.content_length or 0)

        lock = self.manager.lock(state, workspace, lock_info)
        if lock.error:
            if lock.lock_info:
                resp.body = lock.lock_info
                resp.status = falcon.HTTP_CONFLICT
            else:
                resp.body = lock.error
                resp.status = falcon.HTTP_INTERNAL_SERVER_ERROR
            return
        resp.status = falcon.HTTP_OK

    def on_unlock(self, req, resp, state):
        """Handle State Resource UNLOCK requests.

        Args:
            req (object): Terraform HTTP request.
            resp (object): Antichambre HTTP response.
            state (str): Terraform state path.
        """
        workspace = req.params.get("workspace")
        lock_info = req.stream.read(req.content_length or 0)

        unlock = self.manager.unlock(state, workspace, lock_info)
        if unlock.error:
            if unlock.lock_info:
                resp.status = falcon.HTTP_CONFLICT
            else:
                resp.body = unlock.error
                resp.status = falcon.HTTP_INTERNAL_SERVER_ERROR
            return
        resp.status = falcon.HTTP_OK

    def on_options(self, _req, resp, state):
        """Handle State Resource OPTIONS requests.

        Args:
            _req (object): Terraform HTTP request.
            resp (object): Antichambre HTTP response.
            state (str): Terraform state path.
        """
        workspaces = self.manager.workspaces(state)
        if workspaces.error:
            resp.body = workspaces.error
            resp.status = falcon.HTTP_INTERNAL_SERVER_ERROR
            return
        resp.body = json.dumps(workspaces.workspaces)
        resp.status = falcon.HTTP_OK

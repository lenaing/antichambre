"""Vault Storage handling classes"""
import hvac
import requests


class SecretOperationResult():
    """A secret operation result.

    Attributes:
        operation (str): The operation that was performed.
        error (str): Why the operation failed, if it did. `None` otherwise.
    """

    # pylint: disable=too-few-public-methods
    def __init__(self, operation, error=None):
        self.operation = operation
        self.error = error


class RetrieveSecretResult(SecretOperationResult):
    """A secret retrieve operation result.

    Attributes:
        found (bool): Was secret found in Vault.
        secret (str): Secret data.
    """

    # pylint: disable=too-few-public-methods
    def __init__(self, found=True, secret = None, error=None):
        super().__init__("Retrieve", error=error)
        self.found = found
        self.secret = secret

class StoreSecretResult(SecretOperationResult):
    """A secret store operation result."""

    # pylint: disable=too-few-public-methods
    def __init__(self, error=None):
        super().__init__("Store", error=error)

class DeleteSecretResult(SecretOperationResult):
    """A secret delete operation result."""

    # pylint: disable=too-few-public-methods
    def __init__(self, error=None):
        super().__init__("Delete", error=error)

class ListSecretsResult(SecretOperationResult):
    """A secrets list operation result.

    Attributes:
        secrets_list (list(str)): A list of secrets name.
    """

    # pylint: disable=too-few-public-methods
    def __init__(self, secrets_list = None, error=None):
        super().__init__("List", error=error)
        self.secrets_list = secrets_list or []


class VaultStorage:
    """Handle Vault secret storage requests."""

    def __init__(self):
        self.vault_client = hvac.Client(url='http://localhost:8200')

    def store_secret(self, path, secret):
        """Store a secret in Vault.

        Args:
            path (str): Vault secret path.
            secret (dict): Vault secret data.

        Returns:
            StoreSecretResult: Result of store operation.
        """
        try:
            self.vault_client.secrets.kv.v2.create_or_update_secret(
                path=path,
                secret=secret,
            )
        except hvac.exceptions.Forbidden:
            # Permission denied to store a secret in Vault
            return StoreSecretResult(error="Permission Denied")
        except requests.exceptions.ConnectionError:
            # Failed to connect to Vault
            return StoreSecretResult(error="Failed to connect to Vault")
        return StoreSecretResult()

    def read_secret(self, path):
        """Read a secret from Vault.

        Args:
            path (str): Vault secret path.

        Returns:
            StoreSecretResult: Result of read operation.
        """
        try:
            secret_metadata = self.vault_client.secrets.kv.v2.read_secret_version(path=path)
            secret = secret_metadata["data"]["data"]
        except hvac.exceptions.InvalidPath:
            # On Vault side, this can both mean that the path truly doesn't exist or that
            # we don't have permission to view a specific path.

            # We suppose that we have correct permission, therefore it can mean two things :
            # * Either the secret does not exists
            # * Either the last version of a secret does not exists

            # In any case, we consider that the state is gone.
            return RetrieveSecretResult(found=False)
        except hvac.exceptions.Forbidden:
            # Permission denied to read a secret from Vault
            return RetrieveSecretResult(error="Permission Denied")
        except requests.exceptions.ConnectionError:
            # Failed to connect to Vault
            return RetrieveSecretResult(error="Failed to connect to Vault")

        return RetrieveSecretResult(secret=secret)

    def delete_secret(self, path):
        """Delete a secret from Vault.

        Args:
            path (str): Vault secret path.

        Returns:
            StoreSecretResult: Result of delete operation.
        """
        try:
            self.vault_client.secrets.kv.v2.delete_metadata_and_all_versions(path=path)
        except hvac.exceptions.Forbidden:
            # Permission denied to delete a secret from Vault
            return DeleteSecretResult(error="Permission Denied")
        except requests.exceptions.ConnectionError:
            # Failed to connect to Vault
            return DeleteSecretResult(error="Failed to connect to Vault")

        return DeleteSecretResult()

    def list_secrets(self, path):
        """List secrets from Vault.

        Args:
            path (str): Vault secrets path.

        Returns:
            StoreSecretResult: Result of list operation.
        """
        try:
            secrets_list_metadata = self.vault_client.secrets.kv.v2.list_secrets(path=path)
            secrets_list = secrets_list_metadata["data"]["keys"]
        except hvac.exceptions.InvalidPath:
            # On Vault side, this can both mean that the path truly doesn't exist or that
            # we don't have permission to view a specific path.

            # We suppose that we have correct permission, therefore it can mean two things :
            # * Either the secret does not exists
            # * Either the last version of a secret does not exists

            # In any case, we consider that the state is gone.
            return ListSecretsResult(secrets_list=[])
        except hvac.exceptions.Forbidden:
            # Permission denied to list secrets from Vault
            return ListSecretsResult(error="Permission Denied")
        except requests.exceptions.ConnectionError:
            # Failed to connect to Vault
            return ListSecretsResult(error="Failed to connect to Vault")

        return ListSecretsResult(secrets_list=secrets_list)

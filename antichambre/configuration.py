"""Antichambre configuration utilities"""
import os
import toml

def _load_config():
    """
    Load default configuration from packaged TOML file
    and override it with user provided configuration file.
    """
    config = toml.load(os.path.join(os.path.dirname(__file__), 'config.toml'))
    try:
        user_config = toml.load(os.getenv('ANTICHAMBRE_CONFIG_FILE', 'config.toml'))
        config = {**config, **user_config}
    except FileNotFoundError:
        # Config file wasn't found
        pass
    _export_config_to_env(config)
    return config

def _export_config_to_env(config, base="ANTICHAMBRE"):
    """Export current configuration elements to process environment."""
    for key, value in config.items():
        if isinstance(value, dict):
            _export_config_to_env(value, base=f"{base}_{key.upper()}")
        else:
            os.environ[f"{base}_{key.upper()}"] = str(value)
